﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace ClientNanoFramework.Extensions
{
    public static class Extensions
    {
        public static string GetBody(this HttpWebResponse webResponse, Stream stream, int size = 2048)
        {
            using (stream)
            {
                byte[] buffer = new byte[size];
                int bytesRead = 0;
                StringBuilder myStringBuilder = new StringBuilder();


                do
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                    myStringBuilder.Append(Encoding.UTF8.GetString(buffer, 0, bytesRead));
                }
                while (bytesRead >= buffer.Length);

                return myStringBuilder.ToString();
            }
        }
    }
}
