﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Runtime.Remoting;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using ClientNanoFramework.Extensions;
using ClientNanoFramework.SimpleWebServer;
using nanoFramework.Networking;
using nanoFramework.Json;

namespace ClientNanoFramework
{

    public class Program
    {
        public static void Main()
        {
            // Connect to Wi-Fi
            NetworkHelpers.SetupAndConnectNetwork();
                
            Console.WriteLine("Waiting for network up and IP address...");
            NetworkHelpers.IpAddressAvailable.WaitOne();

            // Start webserver
            WebServer ws = new WebServer("http", 80);
            ws.On(request => Resources.GetString(Resources.StringResources.index), "/");
            ws.On(request => "test", "/test");
            ws.On(request => "test2", "/test2");

            ws.Run();

        }
    }
}
