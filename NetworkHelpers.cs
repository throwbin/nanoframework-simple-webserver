﻿//
// Copyright (c) 2019 The nanoFramework project contributors
// See LICENSE file in the project root for full license information.
//

using nanoFramework.Runtime.Events;
using System;
using System.Net.NetworkInformation;
using System.Threading;

namespace nanoFramework.Networking
{
    public class NetworkHelpers
    {
        private const string c_SSID = "";
        private const string c_AP_PASSWORD = "";

        private static bool _requiresDateTime;


        public static ManualResetEvent IpAddressAvailable = new ManualResetEvent(false);
        public static ManualResetEvent DateTimeAvailable = new ManualResetEvent(false);

        internal static void SetupAndConnectNetwork(bool requiresDateTime = false)
        {
            NetworkChange.NetworkAddressChanged += AddressChangedCallback;
            NetworkChange.NetworkAvailabilityChanged += NetworkChange_NetworkAvailabilityChanged;

            _requiresDateTime = requiresDateTime;

            new Thread(WorkingThread).Start();
        }

        private static void NetworkChange_NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            Console.WriteLine("Network availability changed");
        }

        internal static void WorkingThread()
        {
            NetworkInterface[] nis = NetworkInterface.GetAllNetworkInterfaces();

            if (nis.Length > 0)
            {
                // get the first interface
                NetworkInterface ni = nis[0];

                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                {
                    // network interface is Wi-Fi
                    Console.WriteLine("Network connection is: Wi-Fi");

                    Wireless80211Configuration wc =
                        Wireless80211Configuration.GetAllWireless80211Configurations()[ni.SpecificConfigId];

                    // have to update Wi-Fi configuration
                    wc.Ssid = c_SSID;
                    wc.Password = c_AP_PASSWORD;
                    wc.SaveConfiguration();
                    

                    Console.WriteLine("SSID: " + wc.Ssid);
                }

                ni.EnableAutomaticDns();
                ni.EnableDhcp();

                // check if we have an IP
                CheckIP();


                if (_requiresDateTime)
                {
                    IpAddressAvailable.WaitOne();

                    SetDateTime();
                }

            }
            else
            {
                throw new NotSupportedException("ERROR: there is no network interface configured.\r\nOpen the 'Edit Network Configuration' in Device Explorer and configure one.");
            }
        }

        private static void SetDateTime()
        {
            int retryCount = 30;

            Console.WriteLine("Waiting for a valid date & time...");

            // if SNTP is available and enabled on target device this can be skipped because we should have a valid date & time
            while ((DateTime.UtcNow.Year < 2018) || (DateTime.UtcNow.Year > 2020))
            {
                // force update if we haven't a valid time after 30 seconds
                if (retryCount-- == 0)
                {
                    Console.WriteLine("Forcing SNTP update...");

                    Sntp.UpdateNow();

                    // reset counter
                    retryCount = 30;
                }

                // wait for valid date & time
                Thread.Sleep(1000);
            }

            Console.WriteLine($"We have valid date & time: {DateTime.UtcNow.ToString()}");

            DateTimeAvailable.Set();
        }

        private static void CheckIP()
        {
            Console.WriteLine("Checking for IP");

            NetworkInterface ni = NetworkInterface.GetAllNetworkInterfaces()[0];

            if (ni.IPv4Address != null && ni.IPv4Address.Length > 0)
            {
                if (ni.IPv4Address[0] != '0')
                {

                    Console.WriteLine($"We have and IP: {ni.IPv4Address}");
                    IpAddressAvailable.Set();
                }
            }
        }

        private static void AddressChangedCallback(object sender, EventArgs e)
        {
            CheckIP();
        }
    }
}
