﻿using System;
using System.Collections;
using System.Net;
using System.Text;
using System.Threading;
using nanoFramework.Runtime.Native;

namespace ClientNanoFramework.SimpleWebServer
{
    class WebServer
    {
        private readonly HttpListener _listener;

        internal delegate string ResponderMethod(HttpListenerRequest httpListener);

        private Thread _myThread;

        Hashtable myHashtable = new Hashtable();

        public WebServer(string method, int port)
        {
            _listener = new HttpListener(method, port);
            _listener.Start();
        }

        public void On(ResponderMethod responderMethod, string uri)
        {
            myHashtable.Add(uri, responderMethod);

        }

        public void Run()
        {
            _myThread = new Thread(() =>
                {
                    while (_listener.IsListening)
                    {
                        // ! Blocking !
                        var ctx = _listener.GetContext();

                        try
                        {
                            var del = (ResponderMethod)myHashtable[ctx.Request.RawUrl];
                            if (del != null)
                            {
                                string rstr = del(ctx.Request);
                                byte[] buf = Encoding.UTF8.GetBytes(rstr);

                                if (ctx.Response != null)
                                {
                                    ctx.Response.ContentLength64 = buf.Length;

                                    var stream = ctx.Response.OutputStream;
                                    stream?.Write(buf, 0, buf.Length);

                                }
                            }
                        }
                        catch { }
                        finally
                        {
                            ctx.Response?.OutputStream?.Flush();
                        }
                    }
                });
            _myThread.Start();
        }

        public void Stop()
        {
            _listener.Stop();
            _myThread.Abort();
        }
    }
}
